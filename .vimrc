" Vimrc File
" Created by: Jared Ledvina

set laststatus=2
set t_Co=256
set noshowmode
set nowrap
syntax on
set scrolloff=5
set backspace=indent,eol,start

" set the runtime path to include Plug
call plug#begin()

" Appearance Plugs
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'airblade/vim-gitgutter'
Plug 'scrooloose/nerdtree'
Plug 'pearofducks/ansible-vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'chriskempson/base16-vim'

" Airline settings
let g:airline_left_sep=''
let g:airline_right_sep=''
let g:airline_theme='badwolf'
let base16colorspace=256

" Gitgutter Settings
let g:gitgutter_sign_column_always = 1
highlight clear SignColumn

" All of your Plugs must be added before the following line
call plug#end()

" Assume dark background
set background=dark
highlight ColorColumn ctermbg=7
highlight ColorColumn guibg=Gray

" Set basic settings
set number

if v:version >= 704
  set relativenumber
endif

" Mouse settings
set mousehide

" Tab Settings
set autoindent
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4

" Set split settings
set splitright
set splitbelow

" Paste mode hotkey
set pastetoggle=p
set pastetoggle=<Leader>p

" Remaps
nmap <C-b> :NERDTreeToggle<cr>

" Remaps - Vim Split
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Remaps - Visual mode
"↓ in visual mode, when I press <space>d whatever I delete goes to the
"↓ clipboard, for example, <space>dd would delete the current line and I
"↓ could CTRL-C it into Firefox or whatever
vnoremap <Leader>d "+d

"↓ same, but just copies instead of deleting
vnoremap <Leader>y "+y

"↓ paste from the clipboard
vnoremap <Leader>p "+p
vnoremap <Leader>P "+P

"↓ same as above, but for normal mode
nnoremap <Leader>d "+d
nnoremap <Leader>y "+y
nnoremap <Leader>p "+p
nnoremap <Leader>P "+P

" copy whole file (keeping cursor position)
nnoremap <Leader>c :%y+<CR>

" Remaps - fzf
nmap ; :Buffers<CR>
nmap <Leader>t :Files<CR>
nmap <Leader>r :Tags<CR>

" Interactive Search
set incsearch

" Rememberall for vim
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo

function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

set history=1000
" Instead of reverting the cursor to the last position in the buffer, we
" set it to the first line when editing a git commit message
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

" Keep it short, keep it simple
set colorcolumn=81

" Colorscheme
let base16colorspace=256
colorscheme base16-tomorrow

set ttimeout ttimeoutlen=0

" oh my god, so much better
set clipboard=unnamed

" trim trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e
