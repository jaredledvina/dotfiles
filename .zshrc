# INTERNAL UTILITY FUNCTIONS {{{1
# Returns whether the given command is executable or aliased.
_has() {
  return $( whence $1 >/dev/null )
}
# Returns whether the given statement executed cleanly. Try to avoid this
# because this slows down shell loading.
_try() {
  return $( eval $* >/dev/null 2>&1 )
}
# Returns whether the current host type is what we think it is. (HOSTTYPE is
# set later.)
_is() {
  return $( [ "$HOSTTYPE" = "$1" ] )
}
# Returns whether out terminal supports color.
_color() {
  return $( [ -z "$INSIDE_EMACS" ] )
}

# Base16 Shell
 BASE16_SCHEME="tomorrow"
 BASE16_SHELL="$HOME/.config/base16-shell/base16-$BASE16_SCHEME.dark.sh"
 [[ -s $BASE16_SHELL ]] && . $BASE16_SHELL

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Alias'
alias df="df -h"
alias du="du -h"
alias json="python -mjson.tool"
alias nb="newsbeuter"
alias halyard="sudo /opt/halyard/repo/meta/halyard"

# kubectl zsh completion
if [ "$(command -v kubectl >/dev/null 2>&1)" ] ; then
    source <(kubectl completion zsh)
fi

if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Path enviroment
if [ "$(uname)" = "Darwin" ] ; then
    PATH="$HOME/.bin"
    PATH="$PATH:/usr/bin:/usr/sbin:/bin:/sbin"
    for pkg in coreutils gnu-tar gnu-sed ; do
        new_path=/opt/brew/opt/$pkg/libexec/gnubin
        if [ -e $new_path ] ; then
            PATH="$new_path:$PATH"
        fi
    done
    PATH="/opt/brew/bin:/opt/brew/sbin:$PATH"
    PATH="/opt/brew/opt/python/libexec/bin:$PATH"
    PATH="/opt/asdf/bin:/opt/asdf/shims:$PATH"
    PATH="/opt/puppetlabs/pdk/bin/:$PATH"
    PATH="/usr/local/bin:/usr/local/sbin:$PATH"
    PATH="$PATH:$GOPATH/bin"
else
    PATH="$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
fi

# For the Vim GPG Plugin
GPG_TTY=`tty`

# GPGAgent
if [ -f "${HOME}/.gpg-agent-info" ]; then
  . "${HOME}/.gpg-agent-info"
  export GPG_AGENT_INFO
  export SSH_AUTH_SOCK
fi
export GPG_TTY=$(tty)

# Ad-hoc customizations not controlled via git
if [ -d "${HOME}/.bundles/" ]; then
  for bundle in $HOME/.bundles/* ; do
    source $bundle
  done
fi

export GOPATH="${HOME}/Projects/go"

which rbenv &>/dev/null
if [ $? -eq 0 ]; then
  eval "$(rbenv init -)"
fi

# FZF customizations
## fzf via Homebrew
if [ -e $(brew --prefix)/opt/fzf/shell/completion.zsh ]; then
  source $(brew --prefix)/opt/fzf/shell/key-bindings.zsh
  source $(brew --prefix)/opt/fzf/shell/completion.zsh
fi

## fzf + ag configuration
if _has fzf && _has ag; then
  export FZF_DEFAULT_COMMAND='ag --nocolor -g ""'
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
  export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND"
  export FZF_DEFAULT_OPTS='
  --color fg:242,bg:236,hl:65,fg+:15,bg+:239,hl+:108
  --color info:108,prompt:109,spinner:108,pointer:168,marker:168
  '
fi

if _is Linux; then
  if _color && _try ls --color; then
    alias ls='ls --color'
  fi
fi
