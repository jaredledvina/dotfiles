# Dotfiles

###Setup Instructions
1. Clone this to Projects dir
    git clone git@bitbucket.org:jaredledvina/dotfiles.git ~/Projects/dotfiles
2. Install Homebrew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
3. Install dependacies
    `brew install vim tmux htop python`
4. Install dotdotdot into home directory 
    git clone git://github.com/ingydotnet/....git ~/...
5. Copy dotdotdot config 
    ~/.../bin/... conf ~/Projects/dotfiles/dotdotdot.conf
6. Grab all dot files
    ~/.../bin/... update
7. Install!
    ~/.../bin/... install
